# Bilan du FidMe React Native challenge


### Retour d'expérience
J'avoue ne pas avoir très inspiré pour la mise à jour de l'écran en fonction du statut de la demande...
Dans un cas concret j'aurais certainement récupéré la date de la demande et en fonction d'elle, j'aurais affiché le nombre de jour écoulé et les étapes restantes.


### Points d'améliorations
- Séparer le style des composants et leur créant un fichier de style pour chaque
- Créer un fichier de style globaux pour ceux qui reviennent dans plusieurs composants
- Faire un peut plus de factorisation de code
- Ajouter des constantes pour éviter d'avoir des valeurs 'cachées' au fond du composant
- Et certainement d'autres choses auxquelles je ne pense pas à cet instant...


### Temps
2 h 30 - 3 h 00


### Bonne lecture !
Mathieu Brung