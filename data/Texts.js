export const HEADER_TITLE_BOLD = 'Supprimer ';
export const HEADER_TITLE = 'mon compte';
export const HEADER_MESSAGE = 'Cela nous attriste beaucoup de vous voir partir,\nmais nous respectons votre choix';

export const CONTENT_MESSAGE = 'Voici les étapes de la suppression définitive de votre compte fidme :';

export const WARNING_TITLE = 'IMPORTANT : Cette action est irréversible !';
export const WARNING_MESSAGE = 'Passé un délai de 30 jours, il sera impossible de récupérer toutes vos informations, cartes et points de fidélité fidme.';

export const STEP_1_TITLE = 'Demande de suppression';
export const STEP_1_TEXT = 'E-mail de confirmation\nDélai de 30 jours de réflexion pour annuler';
export const STEP_1_DAY = '1';
export const STEP_2_TITLE = 'Email de rappel à J-7';
export const STEP_2_TEXT = '';
export const STEP_2_DAY = '21';
export const STEP_3_TITLE = 'Suppression définitive';
export const STEP_3_TEXT = 'E-mail de confirmation';
export const STEP_3_DAY = '30';

export const BUTTON_ASK_FOR_DELETE = 'Demander la suppression';
export const BUTTON_CANCEL_DELETE = 'Annuler la suppression';