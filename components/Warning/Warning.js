import React from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Text } from '../Text/text';

const ICON_SIZE = 20;

const Warning = (props) => {
    const { icon, title, message } = props
    return (
        <View>
            <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                <Image
                    source={icon}
                    style={styles.icon}
                />
                <Text>{title}</Text>
            </View>
            <Text style={{ textAlign: 'justify' }}>
                {message}
            </Text>
        </View>
    )
}
const styles = StyleSheet.create({
    icon: {
        height: ICON_SIZE,
        width: ICON_SIZE
    }
});

export default Warning;
