// @flow
import React from 'react';
import { Text as RNText, StyleSheet } from 'react-native';
import { colors, size } from './styles.js';


export const Text = (props) => (
    <RNText
        {...props}
        style={[
            styles.text(
                props.fontSize,
                props.color,
                props.lineHeight,
                props.textAlign,
            ),
            props.style,
        ]}>
        {props.children}
    </RNText>
);

Text.defaultProps = {
    fontSize: 'medium',
    color: 'pepper',
    style: {},
    textAlign: 'auto',
};

const styles = StyleSheet.create({
    text: (fontSize, color, lineHeight, textAlign) => ({
        fontSize: size[fontSize] || size.medium,
        color: colors[color] || colors.pepper,
        lineHeight,
        textAlign: textAlign,
    }),
});
