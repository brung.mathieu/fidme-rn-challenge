import { StyleSheet, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { Text } from '../Text/text';

const DeleteButton = (props) => {
    const { onAskForDelete, askForDelete, text } = props;

    return (
        <TouchableOpacity style={[
            styles.deleteButton,
            {
                backgroundColor: askForDelete ? 'green' : '#FF5723',
                marginBottom: askForDelete ? 20 : 0,
                marginTop: askForDelete ? 0 : 20
            }
        ]}
            onPress={() => onAskForDelete(!askForDelete)}
        >

            <Text style={styles.buttonText} >{text}</Text>

        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    deleteButton: {
        height: 40,
        width: '70%',
        borderRadius: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    buttonText: {
        color: 'white',
        fontWeight: 'bold',
    }
});

export default DeleteButton;