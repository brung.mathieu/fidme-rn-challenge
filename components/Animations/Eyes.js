import React from 'react';
import LottieView from 'lottie-react-native';
import eyes from '../../assets/eyes.json';

const Eyes = () => {
    return (
        <LottieView autoPlay loop source={eyes} style={{ width: 110 }} />
    )
}

export default Eyes;