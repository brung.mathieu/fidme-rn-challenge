import styled from '@emotion/native';
import React, { useState } from 'react';
import { Image, StyleSheet, View } from 'react-native';
import { Text } from '../Text/text';

const MARGIN_TOP = 4;
const MARGIN_VERTICAL = 10;
const ICON_SIZE = 24;
const DAY_WIDTH = 64;
const DAY_COLOR_BG = '#73ADEC';

const Step = (props) => {
    const { icon, title, text, day, first, last } = props;
    const [viewBox, setViewBox] = useState(null);

    const DayLine = styled.View({
        position: 'absolute',
        width: 1,
        backgroundColor: DAY_COLOR_BG,
        top: first ? MARGIN_TOP : 0,
        height: last ? MARGIN_TOP : viewBox + MARGIN_VERTICAL * 2,
        zIndex: -1
    })

    return (
        <View
            style={[
                styles.stepContainer,
                { marginBottom: last ? MARGIN_VERTICAL * 2 : MARGIN_VERTICAL }
            ]}
            onLayout={(event) => {
                const viewBox = event.nativeEvent.layout.height;
                setViewBox(viewBox);
            }}
        >

            <View style={styles.leftContainer}>

                <View style={styles.dayWrapper}>
                    <Text
                        fontSize="small"
                        style={styles.textDay}
                    >
                        JOUR {day}
                    </Text>
                </View>

                <DayLine />
            </View>

            <View style={styles.rightContainer}>

                <Image
                    source={icon}
                    style={styles.icon}
                />

                <View style={styles.textsWrapper}>
                    <Text style={styles.bold}>{title}</Text>
                    <Text style={styles.text}>{text}</Text>
                </View>
            </View>

        </View>
    )
}



const styles = StyleSheet.create({
    stepContainer: {
        marginVertical: MARGIN_VERTICAL,
        flexDirection: 'row'
    },
    leftContainer: {
        flex: 1,
        marginRight: 14,
        alignItems: 'center',
    },
    rightContainer: {
        flex: 3,
        flexDirection: 'row'
    },
    dayWrapper: {
        backgroundColor: DAY_COLOR_BG,
        alignItems: 'center',
        borderRadius: 20,
        width: DAY_WIDTH,
        marginTop: MARGIN_TOP,
        paddingVertical: 2
    },
    textsWrapper: {
        marginRight: 20
    },
    text: {
        flexWrap: 'wrap',
    },
    textDay: {
        color: 'white',
    },
    bold: {
        fontWeight: 'bold',
    },
    icon: {
        height: ICON_SIZE,
        width: ICON_SIZE
    }
});

export default Step;