import React from 'react';
import { ICON_CHECK, ICON_CROSS, ICON_EMAIL } from '../../data/Icons';
import { STEP_1_DAY, STEP_1_TEXT, STEP_1_TITLE, STEP_2_DAY, STEP_2_TEXT, STEP_2_TITLE, STEP_3_DAY, STEP_3_TEXT, STEP_3_TITLE } from '../../data/Texts';
import Step from './Step';

const Steps = () => {
    return (
        <>
            <Step
                icon={ICON_CHECK}
                title={STEP_1_TITLE}
                text={STEP_1_TEXT}
                day={STEP_1_DAY}
                first={true}
                last={false}
            />
            <Step
                icon={ICON_EMAIL}
                title={STEP_2_TITLE}
                text={STEP_2_TEXT}
                day={STEP_2_DAY}
                first={false}
                last={false}
            />
            <Step
                icon={ICON_CROSS}
                title={STEP_3_TITLE}
                text={STEP_3_TEXT}
                day={STEP_3_DAY}
                first={false}
                last={true}
            />
        </>
    )
}

export default Steps;