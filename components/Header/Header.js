import React from 'react';
import { StyleSheet, View } from 'react-native';
import { colors } from '../Text/styles';
import { Text } from '../Text/text';
import Eyes from '../Animations/Eyes';
import { HEADER_MESSAGE, HEADER_TITLE, HEADER_TITLE_BOLD } from '../../data/Texts';

const Header = () => {
    return (
        <View style={styles.headerContainer}>
            <View style={styles.headerTitleWrapper}>
                <Text
                    fontSize="large"
                    textAlign="left"
                    color="pepper"
                    style={styles.bold}
                >
                    {HEADER_TITLE_BOLD}
                </Text>
                <Text textAlign="left" fontSize="large">
                    {HEADER_TITLE}
                </Text>
            </View>

            <Eyes />

            <Text textAlign="center" style={styles.headerTextCenter}>
                {HEADER_MESSAGE}
            </Text>
        </View>
    )
}

const styles = StyleSheet.create({
    headerContainer: {
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
        paddingTop: 20,
        paddingBottom: 30,
        borderBottomWidth: 1,
        borderColor: '#CCC',
    },
    headerTitleWrapper: {
        flexDirection: 'row',
        width: '100%',
        paddingHorizontal: 10
    },
    headerTextCenter: {
        flexWrap: 'wrap',
        width: '70%'
    },
    bold: {
        fontWeight: 'bold',
    }
});

export default Header;