/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useCallback, useState } from 'react';
import { SafeAreaView, ScrollView, StatusBar, StyleSheet, TouchableOpacity, View } from 'react-native';
import styled from '@emotion/native';
import { Text } from './components/Text/text';

import { colors } from './components/Text/styles.js';
import Header from './components/Header/Header.js';
import Warning from './components/Warning/Warning';
import Steps from './components/Step/Steps';
import DeleteButton from './components/Buttons/DeleteButton';
import { ICON_WARNING } from './data/Icons';
import { BUTTON_ASK_FOR_DELETE, BUTTON_CANCEL_DELETE, CONTENT_MESSAGE, WARNING_MESSAGE, WARNING_TITLE } from './data/Texts';


const App = () => {
    const [askForDelete, setAskForDelete] = useState(false);
    const onAskForDelete = useCallback((value) => {
        setAskForDelete(value);
    })

    const renderContent = () => (
        <Content>
            {
                askForDelete ?
                    <DeleteButton
                        onAskForDelete={onAskForDelete}
                        askForDelete={askForDelete}
                        text={BUTTON_CANCEL_DELETE}
                    />
                    :
                    <></>
            }

            <Text textAlign="left" style={{ marginBottom: 20 }}>
                {CONTENT_MESSAGE}
            </Text>

            <Steps />

            <Warning
                icon={ICON_WARNING}
                title={WARNING_TITLE}
                message={WARNING_MESSAGE}
            />

            {
                !askForDelete ?
                    <DeleteButton
                        onAskForDelete={onAskForDelete}
                        askForDelete={askForDelete}
                        text={BUTTON_ASK_FOR_DELETE}
                    />
                    :
                    <></>
            }

        </Content>
    );

    return (
        <SafeAreaView style={styles.backgroundStyle}>
            <StatusBar barStyle={'light-content'} />
            <ScrollView
                contentInsetAdjustmentBehavior="automatic"
                style={styles.backgroundStyle}
            >
                <Header />
                {renderContent()}
            </ScrollView>
        </SafeAreaView>
    );
};


const Content = styled.View({
    paddingHorizontal: 20,
    paddingVertical: 30,
    alignItems: 'center',
});

const styles = StyleSheet.create({
    backgroundStyle: {
        flex: 1,
        backgroundColor: colors.background,
    }
})


export default App;
